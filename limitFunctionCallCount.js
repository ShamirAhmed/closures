function limitFunctionCallCount(callback, counter){

    if(callback === undefined || typeof callback !== 'function' || counter === undefined || typeof counter !== 'number')
        return null;

    else{

        return function callCounter(){

            let callbackReturn = [];

            while (counter-- > 0){
                
                let returnedValue = callback(counter,counter,counter,counter,counter)
                callbackReturn.push(returnedValue === undefined ? null : returnedValue);
            }
            
            return callbackReturn;
        }

    }

}

module.exports = limitFunctionCallCount;