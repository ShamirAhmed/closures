function counterFactory(){

    let counter = 786; // counter variable in a closure scope

    return {
        increment(){
            return counter + 1;
        },

        decrement(){
            return counter - 1;
        }
    };

}

module.exports = counterFactory();