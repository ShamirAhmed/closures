function cacheFunction(callback) {

    if(callback === null || typeof callback !== 'function')
        return null;

    else{

        let cache = {};
        let flag = false;

        return function functionReturn(...argument){

            for(let key in cache){
                //console.log(cache[key], argument);
                if(JSON.stringify(cache[key].sort()) === JSON.stringify(argument.sort())){
                    flag = true;
                    //console.log('true')
                    break;            
                }
            }
            
            if(flag !== true){
                callback(...argument);
                cache['arguments'+Object.keys(cache).length] = argument;
                //console.log(cache);
            }
            
            else{
                //console.log('yes');
                return cache;
            }
            
            return null;

        }

    }

}

module.exports = cacheFunction;