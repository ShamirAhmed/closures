const limitFunctionCallCount = require('../limitFunctionCallCount.js');

const myFunction = limitFunctionCallCount((a,b,c) => a+b+c, 3);

console.log(myFunction());