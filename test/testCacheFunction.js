const cacheFunction = require('../cacheFunction.js');

const myFunction = cacheFunction((a,b) => a+b);

console.log(myFunction(1,1));
console.log(myFunction(1,2));
console.log(myFunction(2,1));